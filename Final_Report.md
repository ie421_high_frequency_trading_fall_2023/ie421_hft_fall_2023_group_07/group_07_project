<h1 align="center"> IE 421 Project Final Report: HFT-Technique Enhanced GPS Grandmaster on Raspberry Pi 4 </h1>
<p align="center"> Hao Liang, Cristian Ocampo-Padilla </p>
<p align="center"> December 16th, 2023 </p>

<h2> Team Member Information</h2>
<b>Cristian Ocampo-Padilla</b><br>

* Senior in Computer Science at the University of Illinois at Urbana-Champaign. I am graduating in May 2024, and have taken courses in artificial intelligence, data mining, and software engineering. I have experience working in industry as a Software and Controls Intern for the Connectivity team at Navistar. My skills include Python(PyTorch, Pandas, and NumPy), C/C++, Java, JavaScript, Assembly, and Linux OS. I am on Linkedin as [Cristian Ocampo-Padilla](https://www.linkedin.com/in/cocampo-padilla/) and I can be reached by email at cocampopadilla@gmail.com.

<b>Hao Liang (hliang17@illinois.edu)</b></br>
* Ph.D. student in Industrial Engineering at UIUC. My research interest focuses on Mixed-Integer Programming, Deep Learning Theory, and Transportation Analytics.
* My LinkedIn: https://www.linkedin.com/in/hao-liang-hl3486/

<h2>1. Project Description </h2>
<p>
Time synchronization is a prominent problem in HFT. It ensures timely receipt and processing of market data, which is crucial for order execution, maintaining the competitiveness of HFT algorithms, and optimizing trading strategies. To facilitate this, the SecureSync PTP Grandmaster Clock [(link)](https://safran-navigation-timing.com/product/securesync-ptp-grandmaster/?model_interest__c=PTP+Grandmaster&product_interest___single_select=Resilient+Timing&utm_term=&utm_campaign=Enterprise+Devices&utm_source=adwords&utm_medium=ppc&hsa_acc=9946183019&hsa_), is designed for high performance/low-latency computing applications in financial, industrial and defense markets. It enhances the level of timing performance so that the management of real-time events over wide areas is drastically improved. However, the high cost of this product—exceeding five thousand dollars—makes it unaffordable for many HFT firms.
</p>
<p>
This core objective of this project is optimizing the replication and distribution of the accuracy of expensive master clocks for a Raspberry Pi 4. We hope to create an accessible and highly performant GPS Grandmaster to compete with the higher priced Grandmasters available. More details can be found in our [project proposal](https://gitlab.engr.illinois.edu/ie421_high_frequency_trading_fall_2023/ie421_hft_fall_2023_group_07/group_07_project/-/blob/main/project_proposal.md?ref_type=heads).
</p>

<h3> Main Objectives: </h3>
<ul>
    <li> 
    <b>High-accuracy timing:</b> Configure a Raspberry Pi as a stratum-1 NTP Server using GPS and PPS, which is able to get time directly from GPS satellites within 100 nanoseconds of real time.
    </li>
    <li>
    <b> Fast and accurate time distribution </b> of the PPS output to a large number of other GPS receivers.
    </li>
    <li> 
    <b> Optimization and turning on the Raspberry Pi:</b> With the Raspberry Pis having ARM-based CPUs, we optimize algorithms for the specific architectures, including: 
    </li>
</ul>

<h2> 2. Technologies </h2>
<h3> 2.1. Hardware </h3>
<table>
    <thead>
        <tr>
            <th>Hardware</th>
            <th>Explanation</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>Raspberry Pi</td>
            <td>We use Raspberry Pi 4 CM B</td>
        </tr>
        <tr>
            <td>GPIO cables</td>
            <td>We use serial ports in this project ([link](https://www.amazon.com/dp/B07GD312VG?ref=ppx_yo2ov_dt_b_product_details&th=1))</td>
        </tr>
    </tbody>
    <tbody>
        <tr>
            <td>GPS navigation antenna</td>
            <td>GPS antennas are omnidirectional. They can receive and transmit signals from and to different directions equally.
([link](https://www.amazon.com/dp/B07R7RC96G?ref_=cm_sw_r_ud_dp_AWZP679WX2MF2RSMNYCS&language=en-US&th=1))</td>
        </tr>
    </tbody>
</table>

<h3>2.2 Programming Languages </h3>
<ul>
    <li>
    Python
    </li>
    <li>
    Linux
    </li>
</ul>

<h3>2.3 Software </h3>
<ul>
    <li>
    PTP server daemon
    </li>
    <li>
    Ansible: We use Ansible to automate Bash scripts and the setup process
    </li>
</ul>

<h3>2.4 Packages </h3>
<ul>
    <li>
    <b>gpsd: </b> a service daemon that monitors GPSes receivers attached to a host computer, making data on the location of the sensors available to be queried on TCP port 2947 of the host computer.
    </li>
    <li>
    <b>gpsd-clients:</b>  this package contains various clients using gpsd. 
    </li>
    <li>
    <b>pps-tools:</b>  includes necessary headers for using PPSAPI kernel interface in user-space applications and several support tools.
    </li>
    <li>
    <b>chrony:</b> a versatile implementation of the Network Time Protocol (NTP), which allows us to synchronize the system clock of Raspberry Pi with NTP servers. 
    </li>
    <li>
    <b>jq: </b>a lightweight and flexible command-line JSON processor, which allows us to slice, filter, map and transform structured data with the same ease 
    </li>
    <li>
    <b>tcpdump: </b>a command line utility that allows you to capture and analyze network traffic going through your system. 
    </li>
</ul>


<h2> 3. Executing the Project </h2>
<p>
For new users, we provide detailed instructions on how to execute the project from the beginning based on our hardware. 
</p>

<h3> 3.1. Initial Preparation</h3>
<ol>
    <li>
     Clone the repository and download the scripts <br>
<code>git clone https://gitlab.engr.illinois.edu/ie421_high_frequency_trading_fall_2023/ie421_hft_fall_2023_group_07/group_07_project.git </code><br>
    </li>
    <li>
    Create a file named “host.ini” in your working directory. Edit this file to add the IP addresses or hostnames of the servers you want to manage. For example:<br>
<code>[RPi4_Raspberry_Pi_OS_64]
172.20.10.5</code><br>
    </li>
</ol>


<h3> 3.2. Hardware Configuration</h3>
<ol>
    <li>
    Solder the GPS antenna header to the GPS module, and connect it with the GPS module
    </li>
    <li> Plug the GPS module into the Raspberry Pi’s GPIO pins. <br>
    VCC —> RPi Pin 4 <br>
    GND —> RPi Pin 6 <br>
    RXD —> RPi Pin 8 <br>
    TXD —> RPi Pin 10 <br>
    PPS —> RPi Pin 12 <br>
    
    ![Serial port configuration](https://cdn.mathpix.com/snip/images/iDGPoIg2YCxMjwyBfaQeo3f6e-X1-M8OduKscMiP9XE.original.fullsize.png)<br>

    ![Raspberry Pi 4 Model B 40-pin diagram](https://www.raspberrypi.com/documentation/computers/images/GPIO-Pinout-Diagram-2.png)<br>
    </li>
</ol>

<h3> 3.3. Initial Setup</h3>
<p>
In this tutorial, we use Raspberry Pi OS (Legacy, 64-bit) for the setup process.
In order to automate the setting, we use ansible script [initial_setup.yaml](Ansible Scripts/initial_setup.yaml) to automate the config of rpis. <br> 
<ol>
    <li>
    Install Ansible on your laptop
    </li>
    <li>
    SSH into your Raspberry Pi
    </li>
    <li>
    Execute the Ansible playbook <br>
    <code>ansible-playbook initial_setup.yaml </code> <br>
    Go to number 3, interface options <br>
    Select "Serial Port"<br>
    Select "No" for "Would you want a shell login over serial, select No"<br>
    Select YES to enable the serial port<br>
    Enter “Tab” and select finish. Please do not reboot the <br>Raspberry Pi when it asks you to do so, it will crush the system.
    </li>
    <li>Reboot your Raspberry Pi (Attention: please do not reboot it directly using terminal comments neither. Instead, reboot using the Raspberry Pi GUI.)
    </li>
</ol>
</p>
<p> Specifically, this script helps:
<ul>
    <li> Install, update, and upgrade the required packages (gpsd, gpsd-clients, pps-tools, chrony, jq, tcpdump) on raspberry pi </li>
    <li> Enable the Serial Port on raspberry pi </li> 
    <li> Add the PPS module and configure the GPIO pin for PPS </li> 
    <li> NTP configuration (allow clients to connect to raspberry pi, setup your own time servers) </li> 
</ul>
</p>

<h3> 3.4. Monitor and Collect the PPS “Offset” Data </h3>
<p>
Once gpsd is working and communicating with chrony, it is easy to monitor the "PPS offset" to get a sense of each time the GPS chip raises the Pulse-per-Second pin "High", generating an interrupt. This is essentially exactly how much more expensive higher end GPS PTP / NTP Grandmasters used in the Financial Services industry also function (albeit with slightly different hardware and possibly other software packages, whether open source or proprietary).
<ol>
    <li> <b> Edit the gpsd configuration file </b> <br>
    Firstly, we try to get the the serial port name<br>
    <code> cd /dev/ </code> <br>
    <code> ls -al </code> <br>
    From the result, find this: “serial0 -> ttyS0”. The “ttyS0”is the serial port name we hope to obtain. Then execute the following comment:<br>
    <code> sudo nano /etc/default/gpsd </code> <br>
    And add the following lines in the file:
    ![code](https://cdn.mathpix.com/snip/images/7VjVTU3eYpwaLXEhWholL-EpHbpClS_gLGbFxuKmJFU.original.fullsize.png)<br>
    </li>
    <li> <b>Add PPS to Chrony and configure the NTP server chrony </b><br>
    Execute the below comment to make GPSD start to boot: <br>
    <code> ln -s /lib/systemd/system/gpsd.service /etc/systemd/system/multi-user.target.wants/ </code> <br>
    Open the config file <br>
    <code> sudo nano /etc/chrony/chrony.conf </code> <br>
    Scroll down an uncomment this line:<br>
    ![log tracking](https://cdn.mathpix.com/snip/images/tCp0icwVgEfFXWubq07zDoufSOyy8YjSDT1G8V4BpHY.original.fullsize.png)<br>
    And add these extra lines to the file: <br>
    <code>refclock SHM 0 refid NMEA offset 0.000 precision 1e-3 poll 3 noselect </code><br>
    <code>refclock PPS /dev/pps0 refid PPS lock NMEA poll 3 </code><br>
    Restart chrony and start collecting the PPS “offset” data,so we can tune that in the chrony config, to get the time spot on. <br>
    <code>sudo systemctl restart chrony</code><br>
    Disconnect your computer with your raspberry pi. SSH into your raspberry pi again. Then execute the following comment:<br>
    <code>sudo cat /var/log/chrony/statistics.log | sudo head -2; sudo cat /var/log/chrony/statistics.log | sudo grep NMEA</code>
    ![log tracking data](https://cdn.mathpix.com/snip/images/76IKZudBOZdekZkdZIEGuKdBokrrQRv_5W7sgNuaZ74.original.fullsize.png)<br>
    For the time of running this, the longer the better (we need as least 10 minutes). Once we have waited as long as we wanted to, paste that whole table into a txt file. Then, upload this table as a google drive spreadsheet:
![table 1](https://cdn.mathpix.com/snip/images/rFEKHrKj1sT7VyKjHb8cYHv-M0Xm856IG6eIAcu1ilw.original.fullsize.png)<br>
![table 2](https://cdn.mathpix.com/snip/images/ncoRs0GUcDbi66pbREFXm315OkJevNMfwEA_RJYRVHI.original.fullsize.png)<br>
Delete columns A to Q of this table. Go to the bottom of column G, compute the average of this column:<br>
![table 3](https://cdn.mathpix.com/snip/images/DOkjWJdJjYUHwW67jsOPyFTD8l_WzM5QSzSd5mM_gHg.original.fullsize.png)
Now go back to the chrony config<br>
<code>sudo nano /etc/chrony/chrony.conf </code> <br>
And change the offset on the NMEA line to the number we found:
![offset](https://cdn.mathpix.com/snip/images/X4yD2R-TQBU30PJEoDssjZf6jMKUA0u5k-Vm1PUfzBw.original.fullsize.png)<br>
    </li>
</ol>
</p>

<h3> 3.5. Testing </h3>
<p> Now our raspberry pi is able to get time directly from GPS satellites within 100 nanoseconds of real time. To test whether our configuration is successful, we can run the following comments:</p>

<code>gpsmon</code><br>
![gpsd](https://cdn.mathpix.com/snip/images/AtNvJSqYVufHnBjWKbycJChaTnvRkY2RwbQzQjpQ0DU.original.fullsize.png)<br>


<code>cgps</code><br>
![cgps](https://cdn.mathpix.com/snip/images/MMISwB4-l-_eVj-WQ4lu11VQthSH5_H_WJJ_tADN7w0.original.fullsize.png)<br>


<code>gpspipe -w | jq ".uSat| select( . != null )"</code><br>
(This command monitors the number of satellites your gps module is able to pick up)

<code>gpspipe -w | jq ".uSat| select( . != null )"</code><br>
(This command looks at the chrony sourcesm which refreshes every 1 second. It also looks at the accuracy of our time.)
![Chrony sources](https://cdn.mathpix.com/snip/images/CO0qal1lz31AzjGGRfxFQkT9YvpGNCFUkln4B5CWXoY.original.fullsize.png)<br>

<h3> 3.6. Troubleshooting </h3>
<h4> 3.6.1. Segmentation Fault </h4>

![segmentation fault](https://cdn.mathpix.com/snip/images/23VfVApXaTEQEvUvUj8MK6fgp3nsqIkgvr6K4zDAz4w.original.fullsize.png)<br>
<p>
“Segmentation Fault” can be caused by various issues. This error typically occurs when a program tries to access a memory segment that it's not allowed to access, or it tries to access memory in a way that is not allowed. Common reasons for segmentation faults include: corrupted micro SD card, corrupted operating system with the kernel (improper updates, system crashes, etc).
To troubleshoot the issue, we recommend the following solutions:
</p>
<ol>
    <li>Use a micro SD card with 16GB or less memory. If possible, please be careful buying from Amazon, because they just aggregate all the cards from all sellers and some sellers have fakes. </li>
    <li> Reinstall the system. For new users of Raspberry Pi, we recommend using the Raspberry Pi OS (Legacy, 64-bit):
    <ul>
        <li>Compared to the recommended Raspbian 64-bit System on Raspberry Pi Imager, it generally resolves the segmentation fault issue.
        </li>
        <li> It has a desktop environment, which is easier for new users to operate on.
        </li>
        <li>Most packages support running in a 64-bit environment. We don't need to worry about compatibility issues or other bugs occurring during the installation process.
        </li>
    </ul>
</li>
</ol>



<h4> 3.6.2. Packages have no installation candidates </h4>

![no installation candidates](https://cdn.mathpix.com/snip/images/sZ1ohGaY3wz8PxFYsyzgfobcIW3jMVYFRP_P7FaOBhk.original.fullsize.png)<br>
<p>This message on a Debian-based system typically occurs when installing a software package that is not available in your current repositories, or when there's a mismatch between the system architecture and the package's architecture. To diagnose and resolve the issue, users can:
</p>
<ol>
    <li>Reinstall the system. Sometimes this error message may be caused by system-level issues (e.g., some packages may be incompatible with the current OS; there are conflicting software or libraries). However, our recommended debian system 
    </li>
    <li> Look for other packages with the same functionality as a substitute. Some packages may stop to update.
    </li>
</ol>

<h4> 3.6.3. PPS connection timed out </h4>

![connection timed out](https://cdn.mathpix.com/snip/images/PER01h4ooz7krqKYocXoO4xO9D3hC84hsspKzmvoe-o.original.fullsize.png)<br>
<p>This means that the GPS module is not receiving signals. To troubleshoot this issue, we recommend the following solutions:
</p>
<ol>
    <li>Go to a location where the device can clearly see the sky. GPS antenna receives signals from the satellites.
    </li>
    <li>Wait until the GPS module blinks. It can take around 10 minutes for the GPS to start receiving signals.
    </li>
    <li>Try to get a better GPS Antenna. Usually, we are able to get okay results with the stock one. However, once we go to place it somewhere the signal is poor, the connection becomes unstable
    </li>
</ol>

<h2> 4. Test Results and Analysis </h2>

![isolated parameter results](Figures/isolated_comparison_std.png)
![isolated parameter results](Figures/isolated_comparison_mean.png)
![isolated parameter results](Figures/isolated_comparison_min.png)
![isolated parameter results](Figures/isolated_comparison_max.png)
<p>
We had some unexpected results. The ideal results for the tests are those smallest in magnitude. And the results we were most interested in was the standard deviation and mean of the estimated offset since this would give us insight as to the stability and accuracy of our time server. When looking at the results we saw that they ended up being rather inconclusive. This can be especially seen when comparing the two baseline measurements. Our intuition tells us that all of the parameters should perform better than the stock baseline measurements. When compared to the second baseline measurement, we see that for the most part they all have a smaller magnitude. But then, we compare to the first baseline measurement and we see the opposite trend where the first baseline ends up being better in almost every test. Thus, we do not think the data is reliable enough to reference for analysis and instead have come up with alternative methodologies to hopefully increase the precision.
</p>

<h2> 5. Conclusion </h2>
In this project, we have developed techniques to achieve high-precision timing on Raspberry Pi devices. Our work encompasses providing detailed guidance on implementing microsecond-accurate network time protocol (NTP) using Raspberry Pis, in conjunction with Pulse Per Second (PPS) signals from Global Positioning Systems (GPS). Additionally, we have diligently tuned various parameters on our Raspberry Pi setups and optimized specific architectures to enhance timing accuracy.

However, our findings from these tuning remain somewhat inconclusive. To resolve this issue and get better results, we provide the following future improvement directions.

<h3> 5.1. Future Improvements</h3>
<p>
For future experiments I can think of a number of improvements. For starters, I think that it would be useful to do simultaneous tests with two Raspberry Pis at the same location but different configurations (one stock, one modified). This would allow for direct comparison, removing a lot of the environmental differences. Another improvement I can think of is, if the budget permits, to get a signal producer to mimick a satelite. This way you can directly control the signals being sent and can remove that variability. The reasion I recommend these changes is because there can be drastic performance changes based on just the weather or time of day. So if there is a way to decrease the environmental variability, I believe that it would improve the quality of measurements.
</p>

<h2> 6. Postmortem Summary </h2>
<h3> Cristian Ocampo-Padilla </h3>
<ol>
    <li>
    <b> What did you do in this project? </b>
    <ul>
        <li>I modified my raspberry pi to use various configurations and collected data on how the GPS time server performed on the various configurations.</li>
        <li>I did aggregated and formatted the data I collected and did some basic analysis on it.</li>
        <li>I created Ansible scripts for the various configurations I did.</li>
        <li>Provided technical support to help diagnose problems my teammate was having.</li>
    </ul>
    </li> 
    <li>
    <b> What did you learn as a result? </b><br>
    During this project, I learned how to use Ansible. I also learned a lot about starting up a device from scratch as well as modifying low level settings.
    </li>
    <li>
    <b> Is there anything you could improve in this project experience? </b> <br>
    I would create a script to facilitate the data acquisition and pre-parsing part since I was doing it manually and it was a bit tedious at times. I would also change the way I was collecting data since the data I collected ended up not being as useful as I'd hoped.
    </li>
    <!-- <li>
    <b> If you were to continue working on this project, what would you continue to do to improve it, how, and why? </b>
    </li>
    <li>
    <b> What advice do you offer to future students taking this course and working on their semester-long project? Providing detailed thoughtful advice to future students will be weighed heavily in evaluating your responses </b>
    </li> -->
</ol>

<h3> Hao Liang </h3>
<ol>
    <li>
    <b> What did you do in this project? </b><br>
    <ul>
        <li> Develop a beginner-friendly tutorial that provides step-by-step troubleshooting guidance for setting up a Raspberry Pi, tailored to assist new users in overcoming common setup challenges and technical hurdles.
        </li>
        <li> Write an ansible scripts for the initial setup for raspberry pi.
        </li>
    </ul>
    </li>
    <li>
    <b> What did you learn as a result? </b><br>
        <ul>
        <li> As a beginner in computer science and HFT, I have learned many new knowledge in this area. For these valueable skills, I would like to express my appreiciation to Prof. Lariviere and Cristian, for their helpful technical support:
        <ul>
            <li> Write Ansible scripts to automate the tuning and testing on raspberry pi</li>
            <li> Configure a raspberry pi from the beginning</li>
            <li> Troubleshoot system-level issues and conflicts duringt the configuration process</li>
        </ul>
         </li>
        <li> Another thing I learn is the troubleshooting techniques, including using reading documentations from package developers' website, utilizing ChatGPT, and being able to ask questions.</li>
    </ul>
    </li>
    <li>
    <b> Is there anything you could improve in this project experience? </b><br>
    <ul> 
    <li> One thing I would like to improve is the troubleshooting process. Initially, in this project, I tried to investigate and resolve bugs with myself, akin to how a math student might tackle homework problems independently. However, it became evident that seeking insights and assistance from others is markedly more efficient. This experience emphazies the importance of collaborative learning and open communication in  unfamiliar areas, which is valuable for me.</li>
    <li> Another area for improvement is in time management and task execution. At the begging of the project, I hoped to first thoroughly master the necessary skills before engaging in the hands-on aspects of our work. However, I realize that this perfectionalism can sometimes be a slow down our progress. It may be more beneficial to embrace a 'learning-by-doing' approach. </li>
    </ul>
    </li>
    <!-- <li>
    <b> If you were to continue working on this project, what would you continue to do to improve it, how, and why? </b><br>
    </li>
    <li>
    <b> What advice do you offer to future students taking this course and working on their semester-long project? Providing detailed thoughtful advice to future students will be weighed heavily in evaluating your responses </b><br>
    </li> -->
</ol>

