<h1 align="center"> Project Proposal: HFT-Technique Enhanced GPS Grandmaster on Raspberry Pi 4</h1>
<p align="center">Hao Liang, Cristian Ocampo-Padilla</p>

<h2> Team Member Information</h2>
<p>
Cristian Ocampo-Padilla<br>
Email: <a href="mailto:cocampopadilla@gmail.com">cocampopadilla@gmail.com</a><br>
Graduation Semester: Spring 2024 (Expected)<br>
I may pursue Professional Master of Computer Science<br>
</p>
<p>
Hao Liang<br>
Email: <a href="mailto:hliang17@illinois.edu">hliang17@illinois.edu</a><br>
Graduation Semester: Spring 2028 (Expected)<br>
Currently, I am not pursuing other additional degrees at UIUC
</p>
<h2>1. Project Description</h2>

<h3>1.1. Motivation</h3>
<p>
Time synchronization is a prominent problem in HFT, as it was in the railway networks of the late nineteenth and early twentieth centuries (MacKenzie, 2021, P26). It ensures timely receipt and processing of market data, which is crucial for order execution, maintaining the competitiveness of HFT algorithms, and optimizing trading strategies. To facilitate this, the SecureSync PTP Grandmaster Clock (Díaz, 2022), is designed for high performance/
low-latency computing applications in financial, industrial and defense markets. It enhances the level of timing performance so that the management of real-time events over wide areas is drastically improved. However, the high cost of this product—exceeding five thousand dollars—makes it unaffordable for many HFT firms.
  This project aims to mitigate the challenge. Contrast to the high cost of PTP Grandmasters, it is very easy to turn a Raspberry Pi 4 into a GPS grandmaster using only a few simple commands and a GPS receiver from U-Blox, which costs only $ 7 ~ 30. This project aims to enable Raspberry Pi to distribute extremely precise time to others devices over the network, by first leveraging this technique to sync the clock of the raspberry pi to GPS in high precision. We will utilize other common HFT techniques for enhancing the performance and reducing variability / increasing determinism. Then, apply them to utilizing raspberry pi 4s – First as a GPS grandmaster, and then others as PTP based clients which can sync to PTP grandmaster.
</p>


<h3>1.2. Research Problems</h3>
<p>
The core research problem in our project is optimizing the replication and distribution of the accuracy of expensive master clocks for a Raspberry Pi 4. We seek to overcome the key challenges:
</p>


<ul>
    <li>
    <b>GPS reception</b>:  Timing GPS chips are actually made to prefer satellites directly overhead. They have better ability to filter out multipathing and such data. In general, the better the GPS reception, the better the PPS signal. A challenge will be utilizing the "offset" data outputted by the GPS receiver, and corrects the known error when the PPS output will actually come in
    </li>
  	<li>
  	<b>Thermal regulation</b>: The more stable the ambient temperature of the Raspberry Pi, the less variation in timekeeping. It is essential to utilize the custom busy spin code to actually generate heat as part of a "thermal regulation" of the air around the raspberry pi and GPS receiver [6].
    </li>
    <li>
  	<b>Load on the Raspberry Pi</b>: Highly variable loads will make the processor work harder and easier. Harder working processor means more heat, which results in heat means more variability. Reducing the load on the Pi to alleviate this.
    </li>
    <li>
		<b>Fast and accurate distribution</b> of the PPS output to a large number of other GPS receivers, which are connected to a single pin on the Raspberry Pi.
Solving these issues will contribute to the goal of establishing a reliable and affordable nanosecond-accurate time synchronization system.
    </li>
</ul>

<p>
Solving these issues will contribute to the goal of establishing a reliable and affordable nanosecond-accurate time synchronization system.
</p>


<p>







<h2>2. Technical Challenges</h2>

<p>
Implementing HFT techniques on a Raspberry Pi 4 introduces challenges of meeting strict timing requirements. Raspberry Pis are not the most powerful devices, and while this is not necessarily an issue, it is something to take into consideration. Furthermore, with the Raspberry Pis having ARM-based CPUs, we will have to optimize algorithms for the specific architecture which may prove very challenging because of this niche area and more so on ARM-based hardware. Secondly, we believe that achieving determinism will prove to be very challenging. More general challenges for us will be learning various technologies and methods such as learning how to make bash scripts and using Ansible to automate these scripts. We also think that manipulating low-level systems like clock speed, cpu scheduling, interrupt steering, and more will prove challenging as we have not done much of it before.
</p>

<h2>3. Plan</h2>

<h3>3.1. Expected Deliverables</h3>
<ul>
    <li><b>Bare Minimum Target:</b> Build a basic time-synchronization program.</li>
    <li><b>Expected Completion Goals:</b> Build a basic time-synchronization program with good accuracy.</li>
    <li><b>Reach Goals:</b>
    <ul>
        <li> Achieve high accuracy. Include the project Investigation of U-Blox GPS Receiver Time Sync Best Practices and Performance.</li>
        <li>Utilizing the PPS output from MANY GPS receivers, each connected to a single pin on the raspberry pi. We'd need to talk with U-Blox on details, but the intuition is that the earliest or some minimum number toggled would be treated as the signal rather than relying on only a single receiver. If nothing else, getting statistics on how different chips, all located nearby, would be quite interesting.</li>
    </ul></li>
</ul>

<h3>3.2. Detailed Timeline</h3>
<ul>
    <li><b>Milestone 1 (11/03):</b> Finish the readling the websites in Raspberry PI GPS Tutorials. Learn about the ‘basic skills’ listed in the course gitlab website. Summarize questions and ask Prof. Lariviere / classmates.</li>
    <li><b>Milestone 2 (11/10):</b> Finish learning the Slightly More Advanced Techniques and More Advanced Techniques listed in the course gitlab website. Have run preliminary experiments and explore the time-synchronization performance.</li>
    <li><b>Milestone 3 (11/21):</b> Have some results regarding what accuracy we can achieve. Finish the primary coding part.</li>
    <li><b>Milestone 4 (12/04):</b> Polish up unfinished work and prepare for presentation. Have further enhanced ideas improving the accuracy.</li>
    <li><b>Milestone 5 (12/16):</b> Finalize the project report and make the final submission.</li>
</ul>
<h3>3.3. Resources Needed</h3>
<ul>
    <li>Technologies, libraries, frameworks, languages, etc, to be used / investigated
    <ul>
        <li>Bash scripts, Linux, Ansible, PTP server daemon, pps-tools, gpsd</li>
    </ul></li>
    <li>Hardware
    <ul>
        <li>Raspberry Pi’s, GPS receivers</li>
    </ul></li>
    <li>Frameworks, libraries, etc, for CI/CD (automated testing, deployment, etc)
    <ul>
        <li>Python, Ansible, PTP server daemon, pps-tools, gpsd</li>
    </ul></li>
    <li>Does this project have any user interface aspects (GUIs, webpages, etc)? 
    <ul>
        <li>No.</li>
    </ul></li>
    <li>External resources this project may depend
    <ul>
    <li>Access to spare Raspberry Pi’s will be helpful.</li>
    </ul></li>
</ul>

<h3>3.4. Tasks for Each Team Member</h3>
<p><b>Hao Liang:</b></p>
<ol>
    <li> Analyze and optimize the algorithm</li>
    <li> Learn the suggested techniques</li>
</ol>
<table>
    <thead>
        <tr>
            <th>Level</th>
            <th>Techniques</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>Slightly More Advanced Techniques</td>
            <td>Optimizing code to improve performance (stripping out some unnecessary logging, swapping out or eliminating less efficient functions)</td>
        </tr>
        <tr>
            <td>Slightly More Advanced Techniques</td>
            <td>Utilizing the "offset" data outputted by the GPS receiver that corrects for the known error in when the PPS output will actually come</td>
        </tr>
    </tbody>
</table>

<p><b>Cristian Ocampo-Padilla:</b></p>
<ol>
    <li> Ansible script for automating the setup</li>
    <li> Learn the suggested techniques</li>
</ol>
<table>
    <thead>
        <tr>
            <th>Level</th>
            <th>Techniques</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>Slightly More Advanced Techniques</td>
            <td>Tuning other items that can determine variability in run time like the frequency governor or thermal controls on the raspberry pi. Often modern CPUs will dynamically adjust their CPU clock speed in response to both temperature and to real time use</td>
        </tr>
        <tr>
            <td>Slightly More Advanced Techniques</td>
            <td>Switching from interrupt driven to busy poll driven detection of the PPS output (my understanding is these are mapped to memory locations so you can just continiously busy spin in a dedicated core to detect the first clock cycle (every second) where the PPS value goes high, indicating a new on-rise)</td>
        </tr>
        <tr>
            <td>More Advanced Techniques</td>
            <td>Utilizing custom busy spin code on yet other (non critical cores) to actually generate heat as part of a "thermal regulation" of the air around the raspberry pi and GPS receiver. The more thermally stable the environment, theoretically, the better.</td>
        </tr>
    </tbody>
</table>
<p>
<b>Common tasks:</b><br>
Learn the following techniques:
</p>
<ul>
    <li>Basic Techniques
    <ul>
        <li>isol_cpu kernel parameter so that all processes by default run on the 1st of the four raspberry pi cores (leaving the other three free from any processes or software by default)</li>
        <li>Pinning individual time-related processes each to their own dedicated core(s)</li>
        <li>interrupt steering so that PPS output IRQ goes to either the core running whichever daemon handles the PPS (GPSD or chrony presumably) or another dedicated core</li>
    </ul></li>
    <li>Slightly More Advanced Techniques
    <ul>
        <li>Switching from interrupt driven to busy poll driven detection of the PPS output</li>
        <li>examining recompiling some of the libraries / daemons to alter parameters like size vs speed performance</li>
        <li>utilizing special ARM assembly instructions to control caching so that certain data structures and / or code stay in cache and others are never cached.</li>
        <li>Ensuring compilation is targeting the exact chip being used and not just a generic ARM core</li>
    </ul></li>
    <li>More Advanced Techniques
    <ul>
        <li>Utilizing the PPS output from MANY GPS receivers, each connected to a single pin on the raspberry pi. We'd need to talk with U-Blox on details, but the intuition is that the earliest or some minimum number toggled would be treated as the signal rather than relying on only a single receiver. If nothing else, getting statistics on how different chips, all located nearby, would be quite interesting.</li>
    </ul></li>
</ul>

<h2>4. Literature Search</h2>

<p>
We have read many materials on building Raspberry Pi GPS Time Servers. The first stream of literature are tutorials suggested by Prof. Lariviere: [1, 2, 6, 7, 8], which guides us to create a NTP Server using Raspberry Pi’s with GPS and PPS. The second stream of literature are tutorials that help us master the required techniques of this project [10]. Some of these are books (MacKenzie, 2021) and academic papers. For example, (Ulbricht et al., 2021) proposes a Raspberry Pi-based solution that addresses the lack of precise but easy usable and scalable hardware on the time-scheduling network market, which blocks the easy design of large TSN testbeds for research purposes; (Kim et al., 2019) conducts error analysis on Raspberry Pi time-stamping methods and derived analytical expressions for the maximum variance of time-stamping errors; (Kumalasari et al., 2020) improves the accuracy and the precision of GPS timing of Raspberry Pi by adjusting the Kalman filter. This literature helps us develop a deeper understanding of current research areas in the direction of this project. Also, the lectures delivered in the class will give us a better idea of the techniques through the proposal. Although we have learned from the above materials, a more thorough understanding will lead to a more complete picture of what challenges we will encounter and how we will solve them.
</p>



<h2>5. References</h2>
<ol>
    <li>Díaz, R. (2022, October 2). YouTube. Retrieved October 30, 2023, from <a href="https://safran-navigation-timing.com/product/securesync-ptp-grandmaster/?model_interest__c=P TP+Grandmaster&product_interest___single_select=Resilient+Timing&utm_term=&utm_campa ign=Enterprise+Devices&utm_source=adwords&utm_medium=ppc&hsa_acc=9946183019&hsa_"> LINK</a></li>
    <li>domschl/RaspberryNtpServer: <i>Stratum-1 time server with Raspberry Pi and GPS.</i> (n.d.). GitHub. Retrieved October 30, 2023, from <a href="https://github.com/domschl/RaspberryNtpServer">Raspberry Ntp Server</a> </li>
  <li>Kim, S., Koo, K. Y., & Hester, D. (2019, January). <i>Time Synchronization for Wireless Sensors Using Low-Cost GPS Module and Arduino.</i> Frontiers in Built Environment. </li>
    <li>Kumalasari, L. N., Zainudin, A., & Pratiarso, A. (2020, September). <i>An Implementation of Accuracy Improvement for Low-Cost GPS Tracking Using Kalman Filter with Raspberry Pi.</i> <a href="https://ieeexplore.ieee.org/abstract/document/9231778">IEEE.</a>  </li>
  <li>MacKenzie, D. (2021). <i>Trading at the Speed of Light How Ultrafast Algorithms Are Transforming Financial Markets. Princeton University Press.</i> </li>
  <li><i>Microsecond accurate NTP with a Raspberry Pi and PPS GPS.</i> (2021, April 19). Austin's Nerdy Things. Retrieved October 30, 2023, from <a href="https://austinsnerdythings.com/2021/04/19/microsecond-accurate-ntp-with-a-raspberry-pi-and-pp s-gps/"> Microsecond Accurate Ntp With A Raspberry Pi And PPS-GPS.</a> </li>
    <li> <i>Raspberry Pi GPS Time Server with Bullseye – N4BFR Vision.</i> (2021, November 17). N4BFR Vision. Retrieved October 30, 2023, from <a href="https://n4bfr.com/2021/11/raspberry-pi-gps-time-server-with-bullseye/">LINK</a> </li>
    <li>Robinette, R. (n.d.). <i>Build a Pi Time Server With GPS & Kernel PPS.</i> Rob Robinette's. Retrieved October 30, 2023, from <a href="https://robrobinette.com/pi_GPS_PPS_Time_Server.htm">LINK</a>
 </li>
    <li>Ulbricht, M., Acevedo, J., Krdoyan, S., & Fitzek, F. (2021, September). <i>Precise fruits: Hardware supported time-synchronisation on the RaspberryPI.</i> 10.1109/SmartNets50376.2021.9555415 </li>
    <li>Regulate Temperature with a Raspberry Pi Pico — Part 1: Hardware. (2023, June 15). Medium. Retrieved October 30, 2023, from <a href="https://medium.com/@paulo.de.jesus/regulate-temperature-with-a-raspberry-pi-pico-part-1-hardw are-8b2ea2387484">LINK</a> </li>
</ol>