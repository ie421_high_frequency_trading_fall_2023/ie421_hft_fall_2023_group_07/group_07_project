# Group 7 High Frequency Trading Technology Final Report


## Teammates
<b>Cristian Ocampo-Padilla</b><br>
* I am a Senior in Computer Science at the University of Illinois at Urbana-Champaign. I am graduating in May 2024, and have taken courses in artificial intelligence, data mining, and software engineering. I have experience working in industry as a Software and Controls Intern for the Connectivity team at Navistar. My skills include Python(PyTorch, Pandas, and NumPy), C/C++, Java, JavaScript, Assembly, and Linux OS. I am on Linkedin as [Cristian Ocampo-Padilla](https://www.linkedin.com/in/cocampo-padilla/) and I can be reached by email at cocampopadilla@gmail.com.

<b>Hao Liang (hliang17@illinois.edu)</b></br>
* Ph.D. student in Industrial Engineering at UIUC. My research interest focuses on Mixed-Integer Programming, Deep Learning Theory, and Transportation Analytics.
* My LinkedIn: https://www.linkedin.com/in/hao-liang-hl3486/

## Name
Enhanced GPS Grandmaster on Raspberry Pi 4

## Description
Through this project we hope to create an accessible and highly performant GPS Grandmaster to compete with the higher priced Grandmasters available which will cost over $5,000 USD. More details can be found in our [project proposal](project_proposal.md).

## Set Up
We recommend using Raspberry Pi OS (64-bit) on a Raspberry Pi 4 Model B with 8 GB of RAM. If using this configuration, you can set up Ansible and use our [automated setup](Ansible Scripts/initial_setup.yaml). Otherwise, we found [this tutorial](https://blog.networkprofile.org/gps-backed-local-ntp-server/) useful, however you may need to modify the filepaths.<br>
The GPS module and antenna you use should not matter as long as your GPS has prongs to connect to the GPIO pins on the Raspberry Pi. These are the [GPS module](https://a.co/d/icW8E6c), [antenna](https://a.co/d/fcOHWfw), and [jumper wires](https://a.co/d/bOaFhnl) we used. To connect them we connected jumper wire from pins 4, 6, 8, 10, and 12 on the Raspberry Pi 4 Model B to the respective VCC, GND, RXD, TXD,  and PPS pins on the GPS module.<br>
![Raspberry Pi 4 Model B 40-pin diagram](https://www.raspberrypi.com/documentation/computers/images/GPIO-Pinout-Diagram-2.png)

## Usage
After setting up Ansible, you should be able to use any of the playbooks to apply the settings you would like by running `ansible-playbook playbook_name.yaml`. In theory, this combination of playbooks should work best:
<ul>
    <li>[set_chrony_in_ram.yaml](Ansible Scripts/set_chrony_in_ram.yaml)</li>
    <li>[set_chrony_priority_high.yaml](Ansible Scripts/set_chrony_priority_high.yaml)</li>
    <li>[set_cpu_frequency.yaml](Ansible Scripts/set_cpu_frequency.yaml)</li>
    <li>[set_cpu_isolation.yaml](Ansible Scripts/set_cpu_isolation.yaml) If using this script, you will have to manually change some processes like chrony to use a core other than 0.<br>
    To do this for running processes:
    <ol>
        <li> Get the process identifier (pid) with <code>pgrep &lt;process></code>
        <li> Change the CPU affinity for that process with <code>taskset -cp &lt;core_list> &lt;pid></code>
    </ol></li>
    <li>[set_pps_polling_nonstop.yaml](Ansible Scripts/set_pps_polling_nonstop.yaml)</li>
</ul>

## Project status
Incomplete, further investigation required with more precise methodology as described in the [final report](Final_Report.md).
